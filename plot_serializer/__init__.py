"""Framework for serializing plots."""

__author__ = "Michaela Lestakova"
__email__ = "michaela.lestakova@tu-darmstadt.de"
__version__ = "0.2.1"
