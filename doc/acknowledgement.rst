Acknowledgements
================
The Authors would like to thank the Federal Government and the Heads of Government of the Länder, 
as well as the Joint Science Conference (GWK), for their funding and support within the framework 
of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) - project number 442146713.

Many thanks to the contributors listed in `CONTRIBUTORS.md <https://git.rwth-aachen.de/rdm-tools/plot-serializer/-/blob/main/CONTRIBUTORS.md>`_.

The original idea about storing plot data in human and machine readable form, out of which Plot Serializer was born, 
stems from Kevin T. Logan and Tim M. Buchert. Many thanks for the inspiring discussions!