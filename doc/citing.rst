Citing Plot Serializer
======================
Plot Serializer comes with a citation file: `CITATION.cff <https://git.rwth-aachen.de/rdm-tools/plot-serializer/-/blob/main/CITATION.cff>`_.
Find out how to use it `here <https://book.the-turing-way.org/communication/citable/citable-cff.html#how-to-cite-using-citation-cff>`_. 