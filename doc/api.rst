API documentation
=================

.. autosummary::
   :toctree: _autosummary
   :template: custom-module-template.rst
   :recursive:

   plot_serializer

.. toctree::
   :maxdepth: 1
   :hidden:

   indices_tables

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: JSON Specification

   matplotlib_json_spec_0.2.0 


