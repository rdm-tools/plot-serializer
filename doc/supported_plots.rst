Supported Diagram Types
=======================
PloSe is a relatively young package that is being continually developed.
At the current stage, it does not support serializing all diagram types from ``matplotlib``.
However, PloSe supports the most commonly used types of scientific diagrams and the corresponding plotting commands:

* ``plot()`` (2D and 3D)
* ``scatter()`` (2D and 3D)
* ``pie()``
* ``bar()``  
* ``boxplot()``
* ``errorbar()``
* ``hist()``

