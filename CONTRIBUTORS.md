## Original Authors
- Michaela Leštáková (Idea and initial implementation)
- Ning Xia (Idea and initial implementation)

## Contributors
- Jan Groen (first build after prototype as part of a bachelor project)
- Johnas Jahnel (first build after prototype as part of a bachelor project)
- Julius Florstedt (first build after prototype as part of a bachelor project, expanded first build as scientific staff)
- Max Troppmann (first build after prototype as part of a bachelor project)
- Thomas Wu (first build after prototype as part of a bachelor project)